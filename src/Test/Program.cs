using Test.Storage;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddS3Storage(builder.Configuration);

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.MapGet("/config", (IConfiguration configuration) => Results.Ok(new
    {
        AccessKey = configuration["Storage:AccessKey"],
        SecretKey = configuration["Storage:SecretKey"],
        Region = configuration["Storage:Region"],
        Endpoint = configuration["Storage:Endpoint"]
    }))
    .WithTags("Image Upload");

app
    .MapPost("/upload", async (
        IFormFile image,
        IConfiguration configuration,
        IS3Storage s3
    ) =>
    {
        string imageBucketName = configuration["Storage:ImageBucketName"] ??
                                 throw new ArgumentException("Image Bucket Name is invalid");

        string storageKey = Guid.NewGuid().ToString("N");

        await using var stream = image.OpenReadStream();
        await s3.StoreFile(imageBucketName, storageKey, stream);

        return Results.Ok(new
        {
            Bucket = configuration["Storage:ImageBucketName"],
            Key = storageKey,
            UploadedImagePresignedUrl = s3.GetImageUrl(imageBucketName, storageKey)
        });
    })
    .WithTags("Image Upload");

app.Run();
