﻿namespace Test.Storage;

public interface IS3Storage
{
    Task StoreFile(string bucket, string key, Stream content);
    Task<MemoryStream> GetFile(string bucket, string key);
    string GetImageUrl(string bucket, string key);
}
