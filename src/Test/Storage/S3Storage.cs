﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace Test.Storage;

public class S3Storage: IS3Storage
{
    private readonly IAmazonS3 _s3Client;
    private readonly ILogger<S3Storage> _logger;

    public S3Storage(
        IAmazonS3 s3Client,
        ILogger<S3Storage> logger
    )
    {
        _logger = logger;
        _s3Client = s3Client;
    }

    public async Task StoreFile(string bucket, string key, Stream content)
    {
        try
        {
            var uploadRequest = new TransferUtilityUploadRequest
            {
                InputStream = content,
                Key = key,
                BucketName = bucket,
                CannedACL = S3CannedACL.PublicRead
            };

            var fileTransferUtility = new TransferUtility(_s3Client);

            _logger.LogDebug("Going to upload file to S3. Key:{Key}", key);
            await fileTransferUtility.UploadAsync(uploadRequest);
            _logger.LogDebug("Finished uploading file to S3. Key:{Key}", key);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Could not upload file to S3. Key:{Key}", key);
            throw;
        }
    }

    public async Task<MemoryStream> GetFile(string bucket, string key)
    {
        try
        {
            GetObjectRequest request = new()
            {
                BucketName = bucket,
                Key = key
            };

            _logger.LogDebug("Downloading file from S3. Key:{Key}", key);
            using GetObjectResponse response = await _s3Client.GetObjectAsync(request);

            var stream = new MemoryStream();

            await response.ResponseStream.CopyToAsync(stream);
            stream.Position = 0;

            return stream;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Could not read file from S3. Key:{Key}", key);
            throw;
        }
    }

    public string GetImageUrl(string bucket, string key)
    {
        return _s3Client.GetPreSignedURL(new GetPreSignedUrlRequest
        {
            BucketName = bucket,
            Key = key,
            Expires = DateTime.Now.AddHours(1)
        });
    }
}
