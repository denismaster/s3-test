﻿using Amazon.S3;

namespace Test.Storage;

public static class StorageExtensions
{
    public static void AddS3Storage(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IAmazonS3>(sp =>
        {
            var clientConfig = new AmazonS3Config
            {
                AuthenticationRegion = configuration["Storage:Region"],
                ServiceURL = configuration["Storage:Endpoint"],
                ForcePathStyle = true
            };

            string accessKey = configuration["Storage:AccessKey"] ?? string.Empty;
            string secretKey = configuration["Storage:SecretKey"] ?? string.Empty;
            return new AmazonS3Client(
                accessKey,
                secretKey,
                clientConfig
            );
        });

        services.AddScoped<IS3Storage, S3Storage>();
    }
}
