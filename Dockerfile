FROM mcr.microsoft.com/dotnet/aspnet:7.0
COPY src/Test/bin/Release/net7.0/publish/ app/
WORKDIR /app
ENTRYPOINT ["dotnet", "Test.dll"]
